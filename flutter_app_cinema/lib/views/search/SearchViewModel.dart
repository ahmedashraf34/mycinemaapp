import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/models/PopularModel.dart';
import 'package:flutter_app_cinema/models/SearchModel.dart';
import 'package:flutter_app_cinema/models/UpComingModel.dart';

class SearchViewModel extends BaseViewModel {

  List<ResultsListBean3> mSearchResults = [];
  var searchError = false;

  bool loadingSearch = false;



  getSearchMovies(String word) async {
    loadingSearch = true;
    try {
      Response response = await api.searchRequest(word);
      SearchModel model = SearchModel.fromJson(response.data);
      mSearchResults = model.results;
      loadingSearch = false;
      notifyListeners();
    } on DioError catch (e) {
      searchError = true;
      loadingSearch = false;
      print("service search error : ${e.response}");
      notifyListeners();
    }
  }



  clearSearch() {
    mSearchResults = [];
    loadingSearch = false;
  }



}
