import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/base/Locator.dart';
import 'package:flutter_app_cinema/models/SearchModel.dart';
import 'package:flutter_app_cinema/service/ThemeService.dart';
import 'package:flutter_app_cinema/views/search/SearchViewModel.dart';

class SearchView extends SearchDelegate<String> {
  var themeService = locator<ThemeService>();
  var mCurrentSearchWord = "";

  List<ResultsListBean3> mResults = [];

  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return <Widget>[
      query.isEmpty
          ? IconButton(
              tooltip: 'Voice Search',
              icon: Icon(Icons.mic),
              onPressed: () {},
            )
          : IconButton(
              tooltip: 'Clear',
              icon: Icon(Icons.clear),
              onPressed: () {
                query = '';
                mResults = [];
                mCurrentSearchWord = "";
                showSuggestions(context);
              },
            ),
    ];
  }

  @override
  ThemeData appBarTheme(BuildContext context) {
    return themeService.currentTheme;
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return IconButton(
      tooltip: 'Back',
      icon: Icon(
        Platform.isAndroid ? Icons.arrow_back : Icons.arrow_back_ios,
      ),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    return BaseView<SearchViewModel>(
      builder: (context, model, child) {
        var mCurrentQuary = query;
        if (query == "") {
          mCurrentSearchWord = "";
          query = '';
          mResults = [];
          mCurrentSearchWord = "";
          model.clearSearch();
        } else {
          if (mCurrentSearchWord != query) {
            Future.delayed(
              Duration(seconds: 2),
              () {
                if (mCurrentQuary == query) {
                  mCurrentSearchWord = query;
                  model.getSearchMovies(query).then((_) {
                    mResults = model.mSearchResults;
                  });
                }
              },
            );
          }
        }
        return model.loadingSearch
            ? LoadingProgress()
            : model.searchError
                ? model.buildErrorView()
                : _buildSearchView(model);
      },
    );
  }

  Widget _buildSearchView(SearchViewModel model) {
    return ListView.separated(
        itemCount: mResults.length,
        separatorBuilder: (context, index) => Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Divider(
                height: 1,
                color: Colors.grey[200],
              ),
            ),
        itemBuilder: (context, index) {
          return _buildSearchItem(context, model, index);
        });
  }

  Ink _buildSearchItem(BuildContext context, SearchViewModel model, int index) {
    return Ink(
      child: ListTile(
        leading: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.network(
              "${model.imageUrl}${mResults[index].posterPath}",
              height: 60,
              width: 50,
            )
          ],
        ),
        title: Text(
          "${mResults[index].title}",
          style: TextStyle(
              color: Theme.of(context).textTheme.display2.color,
              fontWeight: FontWeight.bold),
        ),
        subtitle: Text(
          "${mResults[index].releaseDate}",
          style: TextStyle(
            color: Theme.of(context).textTheme.display2.color,
          ),
        ),
        onTap: () {
          model.navDetails("${mResults[index].id}");
        },
      ),
    );
  }
}
