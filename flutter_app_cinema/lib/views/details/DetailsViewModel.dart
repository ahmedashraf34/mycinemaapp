import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/models/CastModel.dart';
import 'package:flutter_app_cinema/models/DetailsModel.dart';
import 'package:flutter_app_cinema/models/PopularModel.dart';
import 'package:flutter_app_cinema/models/TrailerModel.dart';
import 'package:flutter_app_cinema/models/UpComingModel.dart';
import 'package:flutter_app_cinema/models/WatchListModel.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailsViewModel extends BaseViewModel {
  bool loadingDetails  = false;
  bool detailsError = false;
  DetailsModel detailsMovies;

  getDetailsMovies(String id) async {
    loadingDetails = true;
    try {
      Response response = await api.detailsRequest(id);
      DetailsModel model = DetailsModel.fromJson(response.data);
      detailsMovies = model;
      loadingDetails = false;
      notifyListeners();
    } on DioError catch (e) {
      detailsError = true;
      loadingDetails = false;
      print("service details error : ${e.response}");
      notifyListeners();
    }
  }

  bool loadingCast  = false;
  bool castError  = false;
  List<CastListBean> cast;

  getCastMovies(String id) async {
    loadingCast = true;
    try {
      Response response = await api.castRequest(id);
      CastModel model = CastModel.fromJson(response.data);
      cast = model.cast;
      loadingCast = false;
      notifyListeners();
    } on DioError catch (e) {
      castError = true;
      loadingCast = false;
      print("service cast error : ${e.response}");
      notifyListeners();
    }
  }

  bool loadingTrailer  = false;
  bool trailerError  = false;
  List<ResultsListBean4> trailerresults;

  getTrailersMovies(String id) async {
    loadingTrailer = true;
    try {
      Response response = await api.trailersRequest(id);
      TrailerModel model = TrailerModel.fromJson(response.data);
      trailerresults = model.results;
      print("sfsdsadsasdadafsa${trailerresults[0].key}");

      loadingTrailer = false;
      notifyListeners();
    } on DioError catch (e) {
      trailerError = true;
      loadingTrailer = false;
      print("service trailer error : ${e.response}");
      notifyListeners();
    }
  }

  bool isWatchList = false;

  addMovieToDb() {
    db.insertMovie(
      WatchListModel(
        movieId: detailsMovies.id.toString(),
        title: detailsMovies.title,
        genre: detailsMovies.genres[0].name,
        image: detailsMovies.posterPath,
        rate: detailsMovies.voteAverage.toString(),
      ),
    );
    isWatchList = true;
    print("success");
    notifyListeners();
  }

  //WatchList
  removeMovieFromDb() {
    db.deleteMovie(detailsMovies.id.toString());
    isWatchList = false;
    print("removed");

    notifyListeners();
  }

  //WatchList
  checkWatchList(String id) {
    db.loadMovies().then((movies) {
      movies.forEach((movie) {
        if (movie.movieId == id) {
          isWatchList = true;
        }
      });
      notifyListeners();
    });
    print("already in");
  }

  launchTrailer() async {
    if (await canLaunch(
        "http://www.youtube.com/watch?v=${trailerresults[0].key}") &&
        trailerresults.length != 0) {
      await launch("http://www.youtube.com/watch?v=${trailerresults[0].key}");
    }
  }

  shareMsg(String msg) {
    Share.share(msg);
  }
}
