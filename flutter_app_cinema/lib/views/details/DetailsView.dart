import 'dart:ui';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/views/details/DetailsViewModel.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class DetailsView extends StatelessWidget {
  final String id;

  DetailsView(this.id);

  @override
  Widget build(BuildContext context) {
    return BaseView<DetailsViewModel>(
      onModelReady: (model) {
        model.checkWatchList(id);
        model.getDetailsMovies(id);
        model.getCastMovies(id);
        model.getTrailersMovies(id);
      },
      builder: (mContext, model, child) {
        return Scaffold(
          floatingActionButton: FloatingActionButton(onPressed:  model.launchTrailer,
            child: Icon(Icons.play_circle_outline,color: Colors.white,size: 35,),
            backgroundColor:  Colors.yellow[900],
          ),
          body: SafeArea(
            child: model.loadingDetails
                ? LoadingProgress()
                : SingleChildScrollView(
                    child: Container(
                      width: model.screenWidth(context),
                      height: model.screenHeight(context),
                      child: Stack(
                        children: <Widget>[
                          buildPositioned2(model, context),
                          buildPositioned(model, context)
                        ],
                      ),
                    ),
                  ),
          ),
        );
      },
    );
  }

  Positioned buildPositioned2(DetailsViewModel model, BuildContext context) {
    return Positioned.fill(
      child: Column(
        children: <Widget>[
          Container(
            height: model.screenHeight(context, dividedBy: 3),
            width: model.screenWidth(context),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.yellow[900], width: 2),
              image: DecorationImage(
                image: CachedNetworkImageProvider(
                    "${model.imageUrl}${model.detailsMovies.backdropPath}",
                    errorListener: () {
                  return LoadingProgress();
                }),
                fit: BoxFit.cover,
              ),
            ),
            child: BackdropFilter(
              filter: ImageFilter.blur(sigmaX: 2.0, sigmaY: 2.0),
              child: Container(
                decoration:
                    new BoxDecoration(color: Colors.white.withOpacity(0.1)),
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Align(
                        alignment: Alignment.centerLeft,
                        child: IconButton(
                          icon: Icon(
                            Icons.arrow_back,
                            size: 30,
                            color: Colors.white,
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Text(
                        model.detailsMovies.title,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 25,
                            shadows: [
                              BoxShadow(
                                color: Colors.black,
                                offset: Offset(1.0, 1.0),
                                blurRadius: 6.0,
                              )
                            ]),
                      ),
                      Container(
                        height: 40,
                        child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: model.detailsMovies.genres.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                "${model.detailsMovies.genres[index].name}",
                                maxLines: 2,
                                style: TextStyle(
                                    color: Colors.white.withOpacity(0.7),
                                    fontSize: 16,
                                    shadows: [
                                      BoxShadow(
                                        color: Colors.black,
                                        offset: Offset(1.0, 1.0),
                                        blurRadius: 6.0,
                                      )
                                    ]),
                                textAlign: TextAlign.center,
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Positioned buildPositioned(DetailsViewModel model, BuildContext context) {
    return Positioned.fill(
      top: 140.0,
      left: 15,
      child: Container(
        width: model.screenWidth(context),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                Card(
                  clipBehavior: Clip.hardEdge,
                  shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(9),
                      side: BorderSide(
                          color: Colors.yellow[900],
                          width: 1,
                          style: BorderStyle.solid)),
                  child: CachedNetworkImage(
                    imageUrl:
                        "${model.imageUrl}${model.detailsMovies.posterPath}",
                    placeholder: (context, url) => LoadingProgress(),
                    fit: BoxFit.fill,
                    width: model.screenWidth(context, dividedBy: 3),
                  ),
                ),
                SizedBox(
                  width: 15,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: Colors.white,
                              offset: Offset(0.0, 0.0),
                              blurRadius: 3.0,
                            )
                          ], shape: BoxShape.circle),
                          child: Icon(
                            Icons.monetization_on,
                            color: Colors.yellow[900],
                          ),
                        ),
                        SizedBox(width: 10),
                        Text(
                          model.detailsMovies.popularity.toString(),
                          style: TextStyle(
                            shadows: [
                              BoxShadow(
                                color: Colors.yellow[900],
                                offset: Offset(0.0, 0.0),
                                blurRadius: 6.0,
                              )
                            ],
                            fontSize: 18,
                            color: Colors.white,
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: Colors.white,
                              offset: Offset(0.0, 0.0),
                              blurRadius: 3.0,
                            )
                          ], shape: BoxShape.circle),
                          child: Icon(
                            Icons.date_range,
                            color: Colors.yellow[900],
                          ),
                        ),
                        SizedBox(width: 10),
                        Text(
                          model.detailsMovies.releaseDate.toString(),
                          style: TextStyle(shadows: [
                            BoxShadow(
                              color: Colors.yellow[900],
                              offset: Offset(0.0, 0.0),
                              blurRadius: 3.0,
                            )
                          ], fontSize: 18, color: Colors.white),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(boxShadow: [
                            BoxShadow(
                              color: Colors.white,
                              offset: Offset(0.0, 0.0),
                              blurRadius: 3.0,
                            )
                          ], shape: BoxShape.circle),
                          child: Icon(
                            Icons.stars,
                            color: Colors.yellow[900],
                          ),
                        ),
                        SizedBox(width: 10),
                        Text(
                          "${model.detailsMovies.voteAverage / 2}",
                          style: TextStyle(shadows: [
                            BoxShadow(
                              color: Colors.yellow[900],
                              offset: Offset(0.0, 0.0),
                              blurRadius: 3.0,
                            )
                          ], fontSize: 18, color: Colors.white),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 85,
                    ),
                    Row(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () {
                            if (model.isWatchList) {
                              model.removeMovieFromDb();
                            } else {
                              model.addMovieToDb();
                            }
                          },
                          child: Column(
                            children: <Widget>[
                              Icon(
                                model.isWatchList
                                    ? Icons.turned_in
                                    : Icons.turned_in_not,
                                color: Colors.yellow[900],
                                size: 28,
                              ),
                              Text(
                                "WatchList",
                                style: TextStyle(
                                    shadows: [
                                      BoxShadow(
                                        color: Colors.white,
                                        offset: Offset(0.0, 0.0),
                                        blurRadius: 6.0,
                                      )
                                    ],
                                    fontSize: 14,
                                    color: Theme.of(context)
                                        .textTheme
                                        .display2
                                        .color),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: 9,
                        ),
                        GestureDetector(
                          onTap: () {
                            model.shareMsg(model.detailsMovies.homepage);
                          },
                          child: Column(
                            children: <Widget>[
                              Icon(
                                Icons.share,
                                color: Colors.yellow[900],
                                size: 28,
                              ),
                              Text(
                                "Share",
                                style: TextStyle(
                                    shadows: [
                                      BoxShadow(
                                        color: Colors.white,
                                        offset: Offset(0.0, 0.0),
                                        blurRadius: 6.0,
                                      )
                                    ],
                                    fontSize: 14,
                                    color: Theme.of(context)
                                        .textTheme
                                        .display2
                                        .color),
                              ),
                            ],
                          ),
                        ),
                        SizedBox(
                          width: model.screenWidth(context, dividedBy: 15),
                        ),
                      ],
                    ),
                  ],
                ),
//                FittedBox(
//                  fit: BoxFit.contain,
//                  child: GestureDetector(
//                    onTap:  model.launchTrailer,
//                    child: Container(
//                      child: Icon(
//                        Icons.play_circle_outline,
//                        size: 65,
//                        color: Colors.yellow[900],
//                      ),
//                      decoration: BoxDecoration(
//                        border: Border.all(color: Colors.yellow[900], width: 4),
//                        shape: BoxShape.circle,
//                        boxShadow: [
//                          BoxShadow(
//                            color: Colors.white,
//                            offset: Offset(0.0, 0.0),
//                            blurRadius: 10.0,
//                          )
//                        ],
//                      ),
//                    ),
//                  ),
//                )
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              height: 60,
              child: model.loadingCast
                  ? LoadingProgress()
                  : model.castError
                      ? model.buildErrorView()
                      : ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: model.cast.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Column(
                                children: <Widget>[
                                  Container(
                                    height: 40,
                                    width: 40,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        color: Colors.white,
                                        border: Border.all(
                                            color: Colors.yellow[900])),
                                    child: ClipOval(
                                      clipBehavior: Clip.hardEdge,
                                      child: CachedNetworkImage(
                                        fit: BoxFit.cover,
                                        imageUrl:
                                            "${model.imageUrl}${model.cast[index].profilePath}",
                                      ),
                                    ),
                                  ),
                                  Text(
                                    model.cast[index].name,
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 11,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                "Overview",
                style: TextStyle(
                    shadows: [
                      BoxShadow(color: Colors.yellow[900], blurRadius: 6.0)
                    ],
                    fontSize: 23,
                    color: Theme.of(context).textTheme.display2.color),
              ),
            ),
            Divider(
              height: 20,
              color: Colors.yellow[900],
              thickness: 3,
              endIndent: 270,
            ),
            Padding(
              padding: const EdgeInsets.only(right: 15),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  model.detailsMovies.overview,
                  style: TextStyle(
                      fontSize: 12,
                      color: Theme.of(context).textTheme.display2.color),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
