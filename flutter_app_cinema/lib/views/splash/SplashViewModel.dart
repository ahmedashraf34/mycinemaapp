import 'package:flutter_app_cinema/base/BaseViewModel.dart';

class SplashViewModel extends BaseViewModel {
  splashTimer() {
    statusBarService.onHideStatusBar();
    Future.delayed(
      Duration(seconds: 2),
      () {
        statusBarService.onShowStatusBar();
        navHome();
      },
    );
  }
}
