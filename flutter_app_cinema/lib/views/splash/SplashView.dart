import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/views/splash/SplashViewModel.dart';

  class SplashView extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
      return BaseView<SplashViewModel>(
        onModelReady: (model) {
          model.splashTimer();
        },
        builder: (context, model, child) {
          return Scaffold(
            body: Scaffold(
              backgroundColor: Colors.purple,
            ),
          );
        },
      );
    }
  }
