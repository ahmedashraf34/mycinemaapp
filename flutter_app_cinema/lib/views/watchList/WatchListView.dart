import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/views/watchList/WatchListViewModel.dart';
import 'package:flutter_app_cinema/widgets/DialogAlert.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class WatchListView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<WatchListViewModel>(
      onModelReady: (model) {
        model.getWatchList();
      },
      builder: (mContext, model, child) {
        return Container(
          margin: EdgeInsets.all(10),
          padding: EdgeInsets.all(10),
          width: model.screenWidth(context),
          child: model.loadingWatchList
              ? LoadingProgress()
              : model.watchListError
                  ? model.buildErrorView()
                  : model.watchListMoves.length == 0
                      ? buildCenter(context)
                      : buildPageView(model),
        );
      },
    );
  }

  PageView buildPageView(WatchListViewModel model) {
    return PageView.builder(
      physics: BouncingScrollPhysics(),
      pageSnapping: true,
      itemCount: model.watchListMoves.length,
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            model.navDetails("${model.watchListMoves[index].movieId}");
          },
          child: Padding(
            padding: const EdgeInsets.all(20.0),
            child: FittedBox(
              child: Stack(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15))
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      child: CachedNetworkImage(
                        imageUrl:
                            "${model.imageUrl}${model.watchListMoves[index].image}",
                        width: model.screenWidth(context),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 15, left: 15),
                      child: FlutterRatingBarIndicator(
                        rating:
                            double.parse(model.watchListMoves[index].rate) / 2,
                        itemCount: 5,
                        itemSize: 30.0,
                        fillColor: Colors.yellow[900],
                        itemPadding: EdgeInsets.all(3),
                        emptyColor: Colors.amber.withAlpha(50),
                      ),
                    ),
                  ),
                  Positioned(
                    right: 0.0,
                    child: Container(
                      padding: EdgeInsets.all(2),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(bottomLeft: Radius.circular(15),topRight: Radius.circular(15)),
                        color: Colors.yellow[900]
                      ),
                      child: IconButton(
                        icon: Icon(Icons.close,color: Colors.white,size: 30,),
                        onPressed: () {
                          _showRemoveAlert(
                              context, model, index, model.watchListMoves[index].movieId);
                        },
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }

  Center buildCenter(BuildContext context) {
    return Center(
      child: Text(
        "Your WatchList Is Empty \n Please Go And Add Some Movies",
        style: TextStyle(
            height: 2,
            letterSpacing: 2,
            wordSpacing: 1,
            fontSize: 15,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).accentColor),
        textAlign: TextAlign.center,
      ),
    );
  }
  void _showRemoveAlert(
      BuildContext context, WatchListViewModel model, int position, String id) {
    showDialog<bool>(
      context: context,
      builder: (BuildContext context) => DialogAlert(
        cancel: "cancel",
        content: "Do You Want To Remove This Movie From The Watchlist ?",
        ok: "Remove",
        onCancel: () {
          model.navigationService.goBack(results: false);
        },
        onOk: () {
          model.removeWatchList(position, id);
        },
      ),
    ).then((success) {
      print(success);
    });
  }
}
