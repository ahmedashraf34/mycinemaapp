import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/models/PopularModel.dart';
import 'package:flutter_app_cinema/models/UpComingModel.dart';
import 'package:flutter_app_cinema/models/WatchListModel.dart';

class WatchListViewModel extends BaseViewModel {
  var watchListError = false;
  var loadingWatchList = false;
  List<WatchListModel> watchListMoves = [];


  getWatchList() async {
    loadingWatchList = true;
    try {
      db.loadMovies().then((list) {
        watchListMoves = list;
        loadingWatchList = false;
        notifyListeners();
      });
    } on DioError catch (e) {
      watchListError = true;
      loadingWatchList = false;
      print("service watchlist error : ${e.response}");
      notifyListeners();
    }
  }

//Watchlist
  removeWatchList(int index,String id){
    navigationService.goBack(results: true);
    db.deleteMovie(id);
    watchListMoves.removeAt(index);
    notifyListeners();
  }
}
