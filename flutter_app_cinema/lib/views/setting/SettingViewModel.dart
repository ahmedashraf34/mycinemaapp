import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:share/share.dart';

class SettingViewModel extends BaseViewModel {

  shareMsg(String msg) {
    Share.share(msg);
  }
}
