import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/views/setting/SettingViewModel.dart';

class SettingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<SettingViewModel>(
      onModelReady: (model) {
      },
      builder: (mContext, model, child) {
        return Scaffold(
          appBar: AppBar(
            title: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 5),
                  child: Icon(
                    Icons.settings,
                    color: Theme.of(context).textTheme.display2.color,
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Text("Setting"),
              ],
            ),
          ),
          body: SafeArea(
            child: Column(
              children: <Widget>[
                Ink(
                  child: ListTile(
                    onTap: () {
                      model.shareMsg("app/link");
                    },
                    leading: Icon(
                      Icons.share,
                      color: Theme.of(context).textTheme.display2.color,
                    ),
                    title: Text(
                      "Share",
                      style: TextStyle(
                        color: Theme.of(context).textTheme.display2.color,
                      ),
                    ),
                  ),
                ),
                Ink(
                  child: ListTile(
                    onTap: () {

                    },
                    leading: Icon(
                      Icons.error,
                      color: Theme.of(context).textTheme.display2.color,
                    ),
                    title: Text(
                      "About",
                      style: TextStyle(
                        color: Theme.of(context).textTheme.display2.color,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
