import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/views/upcoming/UpcomingViewModel.dart';

class UpcomingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<UpcomingViewModel>(
      onModelReady: (model) {
        model.getUpcomingMovies();
      },
      builder: (mContext, model, child) {
        return Container(
          width: model.screenWidth(context),
          child: model.loadingUpcoming
              ? LoadingProgress()
              : model.upcomingError
                  ? model.buildErrorView()
                  : buildGridView(model),
        );
      },
    );
  }

  GridView buildGridView(UpcomingViewModel model) {
    return GridView.builder(
      itemCount: model.upcomingMovies.length,
      scrollDirection: Axis.vertical,
      gridDelegate:
          SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index) {
        return GestureDetector(
          onTap: () {
            model.navDetails(model.upcomingMovies[index].id.toString());
          },
          child: Stack(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                  child: ClipRRect(
                    child: CachedNetworkImage(
                      width: model.screenWidth(context),
                      fit: BoxFit.cover,
                      imageUrl:
                          "${model.imageUrl}${model.upcomingMovies[index].posterPath}",
                      errorWidget: (context, url, error) => Image.asset(""),
                      placeholder: (context, url) =>

                          Center(child: CircularProgressIndicator()),
                    ),
                    borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                  ),
                ),
              ),
              Positioned(
                bottom: 0.0,
                right: 7.0,
                child: Container(
                  padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                    color: Colors.yellow[900],
                    borderRadius: BorderRadius.all(Radius.elliptical(7, 7)),
                  ),
                  child: Text(
                    model.upcomingMovies[index].releaseDate.toString(),
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 16,
                      shadows: [
                        BoxShadow(
                          color: Colors.black,
                          offset: Offset(1.0, 1.0),
                          blurRadius: 6.0,
                        )
                      ],
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
