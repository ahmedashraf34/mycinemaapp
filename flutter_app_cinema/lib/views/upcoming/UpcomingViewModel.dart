import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/models/PopularModel.dart';
import 'package:flutter_app_cinema/models/UpComingModel.dart';

class UpcomingViewModel extends BaseViewModel {


  var loadingUpcoming = false;
  var upcomingError = false;
  List<ResultsListBean2> upcomingMovies;

  getUpcomingMovies() async {
    loadingUpcoming = true;
    try {
      Response response = await api.upComingRequest();
      UpComingModel model = UpComingModel.fromJson(response.data);
      upcomingMovies = model.results;
      loadingUpcoming = false;
      notifyListeners();
    } on DioError catch (e) {
      upcomingError = true;
      loadingUpcoming = false;
      print("service popular error : ${e.response}");
      notifyListeners();
    }
  }
}
