import 'package:flutter_app_cinema/base/BaseViewModel.dart';

class HomeViewModel extends BaseViewModel {
  String checkPage = "";
  int currentIndex = 0;

  void onTabTapped(int index) {
    currentIndex = index;
    notifyListeners();
  }

  bool chnageColor1 = false;

  onChangeColor1() {
    chnageColor1 = false;
    chnageColor2 = true;
    chnageColor3 = true;
    checkPage = "Discover";
    notifyListeners();
  }

  bool chnageColor2 = true;

  onChangeColor2() {
    chnageColor2 = false;
    chnageColor1 = true;
    chnageColor3 = true;
    checkPage = "Upcoming";
    notifyListeners();
  }

  bool chnageColor3 = true;

  onChangeColor3() {
    chnageColor3 = false;
    chnageColor1 = true;
    chnageColor2 = true;
    checkPage = "WatchList";
    notifyListeners();
  }


  bool isChanged = false;

  onChangeSwitch() {
    if (isChanged) {
      pref.userTheme = "dark";
      onChangeTheme("dark");
      isChanged =false;
      notifyListeners();
    } else {
      pref.userTheme = "light";
      onChangeTheme("light");
      isChanged =true;
      notifyListeners();
    }
    statusBarService.onChangeStatusBarTextColor(pref.userTheme);
    notifyListeners();
  }


















/*
  GetFirstChannel
   */
//  static const batteryChannel = const MethodChannel('battery');
//  static const toastChannel = const MethodChannel('toast');
//  String mBatteryLevel = "i will show you ";
//  Future<void> getBatteryInformation() async {
//    String batteryPercentage;
//    try {
//      var result = await batteryChannel.invokeMethod("getBatteryLevel");
//      batteryPercentage = 'Battery level at $result%';
//    } on PlatformException catch (e) {
//      batteryPercentage = "Failed to get battery level: '${e.message}'.";
//    }
//    mBatteryLevel = batteryPercentage;
//    notifyListeners();
//  }
//  Future<void> showToast() async{
//     toastChannel.invokeMethod("getToast");
//  }

}
