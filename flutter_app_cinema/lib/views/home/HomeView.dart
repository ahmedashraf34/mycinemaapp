import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/views/discover/DiscoverView.dart';
import 'package:flutter_app_cinema/views/search/SearchView.dart';
import 'package:flutter_app_cinema/views/setting/SettingView.dart';
import 'package:flutter_app_cinema/views/upcoming/UpcomingView.dart';
import 'package:flutter_app_cinema/views/watchList/WatchListView.dart';
import 'package:flutter_app_cinema/widgets/CustomTapBar.dart';
import 'HomeViewModel.dart';

class HomeView extends StatelessWidget {
  List children = [];

  @override
  Widget build(BuildContext context) {
    return BaseView<HomeViewModel>(
      onModelReady: (model) {
        model.checkPage = "Discover";
        var userTheme = model.pref.userTheme;
        if (userTheme == "light") {
          model.isChanged = true;
        }
      },
      builder: (context, model, child) {
        children = [
          Container(
            child: Column(
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).textTheme.display1.color,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: GestureDetector(
                        onTap: () {
                          showSearch(context: context, delegate: SearchView());
                        },
                        child: Row(
                          children: <Widget>[
                            Icon(
                              Icons.search,
                              size: 30,
                              color: Colors.grey[800],
                            ),
                            Text(
                              "Search",
                              style: TextStyle(color: Colors.grey[800]),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                CustomTapBar(model),
                SizedBox(
                  height: 5,
                ),
                model.checkPage == "Discover"
                    ? Expanded(
                        child: DiscoverView(),
                      )
                    : model.checkPage == "Upcoming"
                        ? Expanded(
                            child: UpcomingView(),
                          )
                        : model.checkPage == "WatchList"
                            ? Expanded(child: WatchListView())
                            : Expanded(
                                child: Container(),
                              ),
              ],
            ),
          ),
          SettingView()
        ];
        ///////////////////////////////////////////////////////
        return Scaffold(
          floatingActionButtonLocation:
              FloatingActionButtonLocation.centerDocked,
          floatingActionButton: FloatingActionButton(
            backgroundColor: Theme.of(context).textTheme.display2.color,
            child: Icon(
              model.isChanged ? Icons.wb_cloudy : Icons.wb_sunny,
              color: Theme.of(context).primaryColor,
            ),
            onPressed: () {
              model.onChangeSwitch();
            },
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: model.currentIndex,
            selectedItemColor: Colors.yellow[900],
            unselectedItemColor: Theme.of(context).textTheme.display2.color,
            backgroundColor: Theme.of(context).primaryColor,
            iconSize: 25,
            selectedFontSize: 15,
            elevation: 10.5,
            items: [
              BottomNavigationBarItem(
                icon: Icon(Icons.home),
                title: Text('Home'),
              ),
              BottomNavigationBarItem(
                  icon: Icon(
                    Icons.settings,
                  ),
                  title: Text('Setting'))
            ],
            onTap: model.onTabTapped,
          ),
          body: SafeArea(child: children[model.currentIndex]),
        );
      },
    );
  }
}
