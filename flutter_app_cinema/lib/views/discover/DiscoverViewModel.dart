import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/models/PopularModel.dart';
import 'package:flutter_app_cinema/models/UpComingModel.dart';

class DiscoverViewModel extends BaseViewModel {
  var loadingPopular = false;
  var popularError = false;
  List<ResultsListBean> popularMovies;

  getPopularMovies() async {
    loadingPopular = true;
    try {
      Response response = await api.popularRequest();
      PopularModel model = PopularModel.fromJson(response.data);
      popularMovies = model.results;
      loadingPopular = false;
      notifyListeners();
    } on DioError catch (e) {
      popularError = true;
      loadingPopular = false;
      print("service popular error : ${e.response}");
      notifyListeners();
    }
  }

  var loadingUpcoming = false;
  var upcomingError = false;
  List<ResultsListBean2> upcomingMovies;

  getUpcomingMovies() async {
    loadingUpcoming = true;
    try {
      Response response = await api.upComingRequest();
      UpComingModel model = UpComingModel.fromJson(response.data);
      upcomingMovies = model.results;
      loadingUpcoming = false;
      notifyListeners();
    } on DioError catch (e) {
      upcomingError = true;
      loadingUpcoming = false;
      print("service popular error : ${e.response}");
      notifyListeners();
    }
  }
}
