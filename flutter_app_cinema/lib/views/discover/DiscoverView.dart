import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';
import 'package:flutter_app_cinema/base/BaseViewModel.dart';
import 'package:flutter_app_cinema/views/discover/DiscoverViewModel.dart';

class DiscoverView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BaseView<DiscoverViewModel>(
      onModelReady: (model)  {
        model.getPopularMovies();
        model.getUpcomingMovies();
      },
      builder: (context, model, child) {
        return SingleChildScrollView(
          child: Container(
            width: model.screenWidth(context),
            child: Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: Align(
                    child: Text(
                      "POPULAR",
                      style: TextStyle(
                        fontSize: 28,
                        color: Theme.of(context).textTheme.display2.color,
                        shadows: [
                          BoxShadow(
                            color: Colors.yellow[900],
                            offset: Offset(1.0, 1.0),
                            blurRadius: 6.0,
                          )
                        ],
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    alignment: Alignment.centerLeft,
                  ),
                ),
                model.loadingPopular
                    ? LoadingProgress()
                    : model.popularError
                        ? model.buildErrorView()
                        : buildFirstList(model),
                Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 10),
                      child: Align(
                        child: Text(
                          "NEW",
                          style: TextStyle(
                            fontSize: 28,
                            color: Theme.of(context).textTheme.display2.color,
                            fontWeight: FontWeight.bold,
                            shadows: [
                              BoxShadow(
                                color: Colors.yellow[900],
                                offset: Offset(1.0, 1.0),
                                blurRadius: 6.0,
                              )
                            ],
                          ),
                        ),
                        alignment: Alignment.centerLeft,
                      ),
                    ),
                    model.loadingUpcoming
                        ? LoadingProgress()
                        : model.upcomingError
                            ? model.buildErrorView()
                            : buildSecondList(model),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Container buildSecondList(DiscoverViewModel model) {
    return Container(
      height: 150,
      child: ListView.builder(
        itemCount: model.upcomingMovies.length,
        scrollDirection: Axis.horizontal,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: (){
              model.navDetails(model.upcomingMovies[index].id.toString());
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ClipRRect(
                child: CachedNetworkImage(
                  height: 100,
                  imageUrl:
                      "${model.imageUrl}${model.upcomingMovies[index].posterPath}",
                  errorWidget: (context, url, error) => Image.asset(""),
                  placeholder: (context, url) => LoadingProgress(),
                ),
                borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
              ),
            ),
          );
        },
      ),
    );
  }

  Container buildFirstList(DiscoverViewModel model) {
    return Container(
      height: 290,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: model.popularMovies.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {
              model.navDetails(model.popularMovies[index].id.toString());
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Stack(
                children: <Widget>[
                  Container(
                    child: ClipRRect(
                      child: CachedNetworkImage(
                        imageUrl:
                            "${model.imageUrl}${model.popularMovies[index].posterPath}",
                        errorWidget: (context, url, error) => Image.asset(""),
                        placeholder: (context, url) => LoadingProgress(),
                      ),
                      borderRadius: BorderRadius.all(Radius.elliptical(10, 10)),
                    ),
                    margin: EdgeInsets.only(bottom: 10),
                  ),
                  Positioned(
                    child: Container(
                      child: Text(
                        model.popularMovies[index].voteAverage.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 18,
                          shadows: [
                            BoxShadow(
                              color: Colors.black,
                              offset: Offset(1.0, 1.0),
                              blurRadius: 6.0,
                            )
                          ],
                        ),
                        textAlign: TextAlign.center,
                      ),
                      padding: EdgeInsets.all(2),
                      decoration: BoxDecoration(
                        color: Colors.yellow[900],
                        borderRadius: BorderRadius.all(Radius.elliptical(3, 3)),
                      ),
                      margin: EdgeInsets.only(left: 30, right: 30),
                    ),
                    left: 25,
                    right: 25,
                    bottom: 0.0,
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
