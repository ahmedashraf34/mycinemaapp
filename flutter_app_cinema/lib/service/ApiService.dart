import 'package:dio/dio.dart';
import 'package:dio_http_cache/dio_http_cache.dart';

class ApiService {
  Dio _dio;
  static DioCacheManager _manager;

  final base = "https://api.themoviedb.org/3/";
  final apiKey = "0fee860a5aec62fab0414c91065a3516";

  ApiService() {
    _dio = Dio()
      ..interceptors.add(DioCacheManager(CacheConfig(baseUrl: base)).interceptor)
      ..interceptors.add(LogInterceptor(
          responseBody: true,
          error: true,
          requestBody: true,
          requestHeader: true));
  }

  static DioCacheManager getCacheManager() {
    if (null == _manager) {
      _manager = DioCacheManager(
          CacheConfig(baseUrl: "https://api.themoviedb.org/3/"));
    }
    return _manager;
  }

  //UpComing
  Future<Response> upComingRequest() {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {"api_key": apiKey, "language": "en-US"};
    return _dio.get(base + "movie/upcoming",
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }

  //Popular
  Future<Response> popularRequest() {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {"api_key": apiKey, "language": "en-US"};
    return _dio.get(base + "movie/popular",
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }

  //Details
  Future<Response> detailsRequest(String id) {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {"api_key": apiKey, "language": "en-US"};
    return _dio.get(base + "movie/" + id,
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }

  //Cast
  Future<Response> castRequest(String id) {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {"api_key": apiKey};
    return _dio.get(base + "movie/" + id + "/credits",
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }

  //Search
  Future<Response> searchRequest(String word) {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {
      "api_key": apiKey,
      "language": "en-US",
      "query": word
    };
    return _dio.get(base + "search/movie",
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }

  //trailers
  Future<Response> trailersRequest(String id) {
    Map<String, String> header = {};
    Map<String, dynamic> body = {};
    Map<String, String> query = {"api_key": apiKey, "language": "en-US"};
    return _dio.get(base + "movie/$id/videos",
        queryParameters: query, options: buildCacheOptions(Duration(days: 1)));
  }
}
