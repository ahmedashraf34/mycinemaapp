import 'package:shared_preferences/shared_preferences.dart';
class PrefService{

  static SharedPreferences _preferences;
  static PrefService _instance;


  static Future<PrefService> getInstance() async {
    if (_instance == null) {
      _instance = PrefService();
    }
    if (_preferences == null) {
      _preferences = await SharedPreferences.getInstance();
    }
    return _instance;
  }

  //user Local

  String get userLocal => _preferences.getString("userLocal") ?? "en";

  set userLocal(String value) => _preferences.setString("userLocal", value);

  //user Theme

  String get userTheme => _preferences.getString("userTheme") ?? "light";

  set userTheme(String value) => _preferences.setString("userTheme", value);

}