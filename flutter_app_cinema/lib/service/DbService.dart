import 'dart:io';
import 'package:flutter_app_cinema/models/WatchListModel.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:sqflite/sqlite_api.dart';

class DbService {
  static Database _database;

  static DbService _instance;

  static Future<DbService> getInstance() async {
    if (_instance == null) {
      _instance = DbService();
    }
    if (_database == null) {
      _database = await initDB();
    }
    return _instance;
  }

  static initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, "TheMovieTheater.db");
    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
          await db.execute("CREATE TABLE Movies ("
              "id INTEGER PRIMARY KEY,"
              "movieId TEXT,"
              "title TEXT,"
              "image TEXT,"
              "rate TEXT,"
              "genre TEXT"
              ")");
        });
  }

  Future insertMovie(WatchListModel newMovie) async {
    await _database.insert("Movies", newMovie.toJson());
  }

  Future<List<WatchListModel>> loadMovies() async {
    var res = await _database.query("Movies");
    List<WatchListModel> list = res.isNotEmpty
        ? res.map((movie) => WatchListModel.fromJson(movie)).toList()
        : [];
    return list;
  }

  Future<WatchListModel> loadMovie(String id) async {
    var res = await _database.query("Movies", where: "movieId = ?", whereArgs: [id]);
    return res.isNotEmpty ? WatchListModel.fromJson(res.first) : Null;
  }

  updateMovie(WatchListModel updatedMovie) async {
    var res = await _database.update("Movies", updatedMovie.toJson(),
        where: "movieId = ?", whereArgs: [updatedMovie.movieId]);
    return res;
  }

  deleteMovie(String id) async {
    _database.delete("Movies", where: "movieId = ?", whereArgs: [id]);
  }

  deleteAllMovies() async {
    _database.rawDelete("Delete * from Movies");
  }
}
