import 'dart:io';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/Locator.dart';

import 'PrefService.dart';

typedef void ThemeChangeCallback(String theme);

class ThemeService {
  ThemeChangeCallback onThemeChanged;
  var pref = locator<PrefService>();
  ThemeData _currentTheme;

  ThemeService() {
    initThemes();
    var theme = pref.userTheme;
    if (Platform.isAndroid) {
      if (theme == "light") {
        _currentTheme = androidLightTheme;
      } else if (theme == "dark") {
        _currentTheme = androidDarkTheme;
      }
    } else if (Platform.isIOS) {
      if (theme == "light") {
        _currentTheme = iosLightTheme;
      } else if (theme == "dark") {
        _currentTheme = iosDarkTheme;
      }
    }
  }

  //Colors

  //Light Theme
  final Color primaryColor_LT = Colors.white;
  final Color primaryColorDark_LT = Colors.white;
  final Color accentColor_LT = Colors.yellow[900];

  final Color textThemeDisplay1_LT = Colors.grey[300];
  final Color textThemeDisplay2_LT = Colors.black;
  final Color textThemeDisplay3_LT = Colors.grey[300];

  //Dark Theme
  final primaryColor_DT = Color(0xff1E1E37);
  final primaryColorDark_DT = Colors.white;
  final accentColor_DT = Colors.white;

  final textThemeDisplay1_DT = Colors.grey[600];
  final textThemeDisplay2_DT = Colors.white;
  final textThemeDisplay3_DT = Colors.grey[600];

  //Themes
  ThemeData androidLightTheme;
  ThemeData androidDarkTheme;
  ThemeData iosLightTheme;
  ThemeData iosDarkTheme;

  //Define Themes

  void initThemes() {
    iosDarkTheme = ThemeData(
      fontFamily: "breef",
      scaffoldBackgroundColor: Colors.white,
      primaryColor: primaryColor_DT,
      primaryColorDark: primaryColorDark_DT,
      accentColor: accentColor_DT,
    );

    iosLightTheme = ThemeData(
      fontFamily: "breef",
      scaffoldBackgroundColor: Colors.white,
      primaryColor: primaryColor_LT,
      primaryColorDark: primaryColorDark_LT,
      accentColor: accentColor_LT,
    );

    androidDarkTheme = ThemeData(
      fontFamily: "breef",
      scaffoldBackgroundColor: primaryColor_DT,
      textTheme: TextTheme(
          display3: TextStyle(color: textThemeDisplay3_DT),
          display1: TextStyle(color: textThemeDisplay1_DT),
          display2: TextStyle(color: textThemeDisplay2_DT)),
      primaryColor: primaryColor_DT,
      primaryColorDark: primaryColorDark_DT,
      accentColor: accentColor_DT,
    );

    androidLightTheme = ThemeData(
      fontFamily: "breef",
      textTheme: TextTheme(
          display3: TextStyle(color: textThemeDisplay3_LT),
          display1: TextStyle(color: textThemeDisplay1_LT),
          display2: TextStyle(color: textThemeDisplay2_LT)),
      primaryColor: primaryColor_LT,
      scaffoldBackgroundColor: primaryColor_LT,
      primaryColorDark: primaryColorDark_LT,
      accentColor: accentColor_LT,
    );
  }

  ThemeData get currentTheme => _currentTheme;

  void changeTheme(String theme) {
    pref.userTheme = theme;
    if (Platform.isAndroid) {
      if (theme == "light") {
        _currentTheme = androidLightTheme;
      } else if (theme == "dark") {
        _currentTheme = androidDarkTheme;
      }
    } else if (Platform.isIOS) {
      if (theme == "light") {
        _currentTheme = iosLightTheme;
      } else if (theme == "dark") {
        _currentTheme = iosDarkTheme;
      }
    }
  }
}
