import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'PrefService.dart';

class StatusBarService {
  //Remove Full Screen
  onShowStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays(SystemUiOverlay.values);
  }

  //Show Full Screen
  onHideStatusBar() {
    SystemChrome.setEnabledSystemUIOverlays([]);
  }

  //Status Bar Theme
  onChangeStatusBarTextColor(String userTheme) {
    if (userTheme == "light") {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.dark.copyWith(
//          systemNavigationBarColor: Colors.red,
          statusBarBrightness:
              Brightness.light // Dark == white status bar -- for IOS.
          ));
    } else if (userTheme == "dark") {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle.light.copyWith(
//          systemNavigationBarColor: Colors.blue,
          statusBarBrightness:
              Brightness.dark // Dark == white status bar -- for IOS.
          ));
    }
  }
}
