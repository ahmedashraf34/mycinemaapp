import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/base/BaseView.dart';


class TestPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 200,
              decoration: new BoxDecoration(
                image: new DecorationImage(
                  image: new NetworkImage("https://images.pexels.com/photos/396547/pexels-photo-396547.jpeg?auto=compress&cs=tinysrgb&h=350"),
                ),
              ),
              child: new BackdropFilter(
                filter: new ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
                child: new Container(
                  decoration: new BoxDecoration(color: Colors.white.withOpacity(0.0)),
                  child: Column(
                    children: <Widget>[
                      Text("safsaf"),
                      Text("safsaf"),
                      Text("safsaf"),
                    ],
                  ),
                ),
              ),
            ),
            Text("sfsaf")
          ],
        ),
      );

  }
}