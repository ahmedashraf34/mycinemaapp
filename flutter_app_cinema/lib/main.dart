import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_cinema/service/LocalizationService.dart';
import 'package:flutter_app_cinema/service/NavigationService.dart';
import 'package:flutter_app_cinema/service/ThemeService.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_app_cinema/base/LifeCycleManager.dart';

import 'base/Locator.dart';
import 'base/Router.dart';
import 'local/AppLocalizations.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  return runApp(
//    DevicePreview(
//      builder: (context) =>
    MyApp(),
//    ),
  );
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<MyApp> {
  var localService = locator<LocalizationService>();
  var themeService = locator<ThemeService>();

  @override
  void initState() {
    super.initState();
    localService.onLocaleChanged = onLocaleChange;
    themeService.onThemeChanged = onThemeChange;
  }

  onLocaleChange(String countryCode) {
    setState(() {
      localService.changeLocal(countryCode);
    });
  }

  onThemeChange(String theme) {
    setState(() {
      themeService.changeTheme(theme);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [

      ],
      child: LifeCycleManager(
          child: MaterialApp(
            localizationsDelegates: [
              GlobalMaterialLocalizations.delegate,
              GlobalWidgetsLocalizations.delegate,
              FallbackCupertinoLocalisationsDelegate(),
              localService.currentLocal
            ],
            supportedLocales: [Locale('en'), Locale('ar')],
            locale: localService.currentLocal.overriddenLocale,
            debugShowCheckedModeBanner: false,
            title: 'App Cinma',
//            builder: DevicePreview.appBuilder, // <--- Add the builder

            theme: themeService.currentTheme,
            onGenerateRoute: Router.generateRoute,
            navigatorKey: locator<NavigationService>().navigatorKey,
          )

      ),
    );
  }
}
