class WatchListModel {
  final String movieId;
  final String title;
  final String image;
  final String rate;
  final String genre;

  WatchListModel({this.movieId, this.image, this.title, this.rate, this.genre});

  factory WatchListModel.fromJson(Map<String, dynamic> json) => new WatchListModel(
    movieId: json["movieId"],
    title: json["title"],
    image: json["image"],
    rate: json["rate"],
    genre: json["genre"],
  );

  Map<String, dynamic> toJson() => {
    "movieId": movieId,
    "title": title,
    "image": image,
    "rate": rate,
    "genre": genre,
  };
}
