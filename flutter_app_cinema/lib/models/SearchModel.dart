class SearchModel {
  int page;
  int totalResults;
  int totalPages;
  List<ResultsListBean3> results;

  SearchModel({this.page, this.totalResults, this.totalPages, this.results});

  SearchModel.fromJson(Map<String, dynamic> json) {    
    this.page = json['page'];
    this.totalResults = json['total_results'];
    this.totalPages = json['total_pages'];
    this.results = (json['results'] as List)!=null?(json['results'] as List).map((i) => ResultsListBean3.fromJson(i)).toList():null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    data['results'] = this.results != null?this.results.map((i) => i.toJson()).toList():null;
    return data;
  }

}

class ResultsListBean3 {
  String title;
  String releaseDate;
  String originalLanguage;
  String originalTitle;
  String backdropPath;
  String overview;
  String posterPath;
  bool video;
  bool adult;
  dynamic popularity;
  dynamic voteAverage;
  int id;
  int voteCount;
  List<int> genreIds;

  ResultsListBean3({this.title, this.releaseDate, this.originalLanguage, this.originalTitle, this.backdropPath, this.overview, this.posterPath, this.video, this.adult, this.popularity, this.voteAverage, this.id, this.voteCount, this.genreIds});

  ResultsListBean3.fromJson(Map<String, dynamic> json) {
    this.title = json['title'];
    this.releaseDate = json['release_date'];
    this.originalLanguage = json['original_language'];
    this.originalTitle = json['original_title'];
    this.backdropPath = json['backdrop_path'];
    this.overview = json['overview'];
    this.posterPath = json['poster_path'];
    this.video = json['video'];
    this.adult = json['adult'];
    this.popularity = json['popularity'];
    this.voteAverage = json['vote_average'];
    this.id = json['id'];
    this.voteCount = json['vote_count'];

    List<dynamic> genreIdsList = json['genre_ids'];
    this.genreIds = new List();
    this.genreIds.addAll(genreIdsList.map((o) => int.parse(o.toString())));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['title'] = this.title;
    data['release_date'] = this.releaseDate;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['backdrop_path'] = this.backdropPath;
    data['overview'] = this.overview;
    data['poster_path'] = this.posterPath;
    data['video'] = this.video;
    data['adult'] = this.adult;
    data['popularity'] = this.popularity;
    data['vote_average'] = this.voteAverage;
    data['id'] = this.id;
    data['vote_count'] = this.voteCount;
    data['genre_ids'] = this.genreIds;
    return data;
  }
}
