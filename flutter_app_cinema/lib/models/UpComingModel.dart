class UpComingModel {
  int page;
  int totalResults;
  int totalPages;
  DatesBean dates;
  List<ResultsListBean2> results;

  UpComingModel({this.page, this.totalResults, this.totalPages, this.dates, this.results});

  UpComingModel.fromJson(Map<String, dynamic> json) {    
    this.page = json['page'];
    this.totalResults = json['total_results'];
    this.totalPages = json['total_pages'];
    this.dates = json['dates'] != null ? DatesBean.fromJson(json['dates']) : null;
    this.results = (json['results'] as List)!=null?(json['results'] as List).map((i) => ResultsListBean2.fromJson(i)).toList():null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['page'] = this.page;
    data['total_results'] = this.totalResults;
    data['total_pages'] = this.totalPages;
    if (this.dates != null) {
      data['dates'] = this.dates.toJson();
    }
    data['results'] = this.results != null?this.results.map((i) => i.toJson()).toList():null;
    return data;
  }

}

class DatesBean {
  String maximum;
  String minimum;

  DatesBean({this.maximum, this.minimum});

  DatesBean.fromJson(Map<String, dynamic> json) {    
    this.maximum = json['maximum'];
    this.minimum = json['minimum'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['maximum'] = this.maximum;
    data['minimum'] = this.minimum;
    return data;
  }
}

class ResultsListBean2 {
  String posterPath;
  String backdropPath;
  String originalLanguage;
  String originalTitle;
  String title;
  String overview;
  String releaseDate;
  bool video;
  bool adult;
  double popularity;
  dynamic voteAverage;
  int voteCount;
  int id;
  List<int> genreIds;

  ResultsListBean2({this.posterPath, this.backdropPath, this.originalLanguage, this.originalTitle, this.title, this.overview, this.releaseDate, this.video, this.adult, this.popularity, this.voteAverage, this.voteCount, this.id, this.genreIds});

  ResultsListBean2.fromJson(Map<String, dynamic> json) {
    this.posterPath = json['poster_path'];
    this.backdropPath = json['backdrop_path'];
    this.originalLanguage = json['original_language'];
    this.originalTitle = json['original_title'];
    this.title = json['title'];
    this.overview = json['overview'];
    this.releaseDate = json['release_date'];
    this.video = json['video'];
    this.adult = json['adult'];
    this.popularity = json['popularity'];
    this.voteAverage = json['vote_average'];
    this.voteCount = json['vote_count'];
    this.id = json['id'];

    List<dynamic> genreIdsList = json['genre_ids'];
    this.genreIds = new List();
    this.genreIds.addAll(genreIdsList.map((o) => int.parse(o.toString())));
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['poster_path'] = this.posterPath;
    data['backdrop_path'] = this.backdropPath;
    data['original_language'] = this.originalLanguage;
    data['original_title'] = this.originalTitle;
    data['title'] = this.title;
    data['overview'] = this.overview;
    data['release_date'] = this.releaseDate;
    data['video'] = this.video;
    data['adult'] = this.adult;
    data['popularity'] = this.popularity;
    data['vote_average'] = this.voteAverage;
    data['vote_count'] = this.voteCount;
    data['id'] = this.id;
    data['genre_ids'] = this.genreIds;
    return data;
  }
}
