import 'package:flutter_app_cinema/service/ApiService.dart';
import 'package:flutter_app_cinema/service/DbService.dart';
import 'package:flutter_app_cinema/service/LocalizationService.dart';
import 'package:flutter_app_cinema/service/NavigationService.dart';
import 'package:flutter_app_cinema/service/PrefService.dart';
import 'package:flutter_app_cinema/service/StatusBarService.dart';
import 'package:flutter_app_cinema/service/ThemeService.dart';
import 'package:flutter_app_cinema/views/details/DetailsViewModel.dart';
import 'package:flutter_app_cinema/views/discover/DiscoverViewModel.dart';
import 'package:flutter_app_cinema/views/home/HomeViewModel.dart';
import 'package:flutter_app_cinema/views/search/SearchViewModel.dart';
import 'package:flutter_app_cinema/views/setting/SettingViewModel.dart';
import 'package:flutter_app_cinema/views/splash/SplashViewModel.dart';
import 'package:flutter_app_cinema/views/upcoming/UpcomingViewModel.dart';
import 'package:flutter_app_cinema/views/watchList/WatchListViewModel.dart';
import 'package:get_it/get_it.dart';


GetIt locator;

setupLocator() async{
  locator = GetIt.instance;
  locator.registerLazySingleton(() => ApiService());
  locator.registerLazySingleton(() => LocalizationService());
  locator.registerLazySingleton(() => ThemeService());
  locator.registerLazySingleton(() => NavigationService());
  locator.registerLazySingleton(() => StatusBarService());
  var prefInstance = await PrefService.getInstance();
  locator.registerSingleton<PrefService>(prefInstance);
  var dbInstance = await DbService.getInstance();
  locator.registerSingleton<DbService>(dbInstance);

  //Register Model Views
  locator.registerFactory(() => SplashViewModel());
  locator.registerFactory(() => HomeViewModel());
  locator.registerFactory(() => DiscoverViewModel());
  locator.registerFactory(() => UpcomingViewModel());
  locator.registerFactory(() => WatchListViewModel());
  locator.registerFactory(() => SettingViewModel());
  locator.registerFactory(() => DetailsViewModel());
  locator.registerFactory(() => SearchViewModel());

}
