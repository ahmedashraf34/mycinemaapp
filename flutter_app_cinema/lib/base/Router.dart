import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_cinema/views/details/DetailsView.dart';
import 'package:flutter_app_cinema/views/home/HomeView.dart';
import 'package:flutter_app_cinema/views/splash/SplashView.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) {
          return HomeView();
        });
      case '/home':
        return MaterialPageRoute(builder: (_) {
          return HomeView();
        });
      case '/details':
        return MaterialPageRoute(builder: (_) {
          var movieId = settings.arguments as String;

          return DetailsView(movieId);
        });
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          ),
        );
    }
  }
}
