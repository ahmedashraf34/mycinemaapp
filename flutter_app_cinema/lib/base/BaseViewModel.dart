import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_cinema/service/ApiService.dart';
import 'package:flutter_app_cinema/service/DbService.dart';
import 'package:flutter_app_cinema/service/LocalizationService.dart';
import 'package:flutter_app_cinema/service/NavigationService.dart';
import 'package:flutter_app_cinema/service/PrefService.dart';
import 'package:flutter_app_cinema/service/StatusBarService.dart';
import 'package:flutter_app_cinema/service/ThemeService.dart';
import 'package:provider/provider.dart';

import 'Locator.dart';

class BaseViewModel extends ChangeNotifier {
  final api = locator<ApiService>();
  final pref = locator<PrefService>();
  final db = locator<DbService>();
  final localService = locator<LocalizationService>();
  final themeService = locator<ThemeService>();
  final navigationService = locator<NavigationService>();
  final statusBarService = locator<StatusBarService>();
  final imageUrl = "http://image.tmdb.org/t/p/w185";



  //Local
  onChangeLang(String countryCode) {
    localService.onLocaleChanged(countryCode);
  }

  //Theme
  onChangeTheme(String theme) {
    themeService.onThemeChanged(theme);
  }

  navHome() {
    navigationService.navigateReplacementTo("/home");
  }
  navDetails(String id) {
    navigationService.navigateTo("/details",arguments: id);
  }
//  Future<File> sendImageCamera(ImageSource source) async {
//    var image = await ImagePicker.pickImage(
//        source: source, maxWidth: 800, maxHeight: 800);
//    return image;
//  }

  Size screenSize(BuildContext context) {
    return MediaQuery.of(context).size;
  }

  double screenHeight(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).height / dividedBy;
  }

  double screenWidth(BuildContext context, {double dividedBy = 1}) {
    return screenSize(context).width / dividedBy;
  }

  Center buildErrorView() {
    return Center(
      child: Text("SomeThing Happpen Please Try Again"),
    );
  }

}
class LoadingProgress extends StatelessWidget {

  LoadingProgress();

  @override
  Widget build(BuildContext context) {
    if (Platform.isAndroid) {
      return Center(child: CircularProgressIndicator());
    } else if(Platform.isIOS){
      return Center(child: CupertinoActivityIndicator(radius: 15,));
    }
  }
}
