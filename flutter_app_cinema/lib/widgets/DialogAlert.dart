import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DialogAlert extends StatelessWidget {
  final String content, cancel, ok;
  final Function onCancel, onOk;

  DialogAlert(
      {@required this.content,
      @required this.cancel,
      @required this.ok,
      @required this.onCancel,
      @required this.onOk});

  @override
  Widget build(BuildContext context) {
    Widget dialog;
    if (Platform.isAndroid) {
      dialog = AlertDialog(
        content: Text(
          content,
          style: TextStyle(color: Colors.black,),
        ),
        actions: <Widget>[
          FlatButton(
            child: Text(cancel,style: TextStyle(color: Colors.grey[800]),),
            onPressed: onCancel,
          ),
          FlatButton(
            child: Text(ok,style: TextStyle(color: Colors.yellow[900]),),
            onPressed: onOk,
          ),
        ],
      );
    } else if (Platform.isIOS) {
      dialog = CupertinoAlertDialog(
        content: Text(
          content,
          style: TextStyle(color: Colors.black,),
        ),
        actions: <Widget>[
          FlatButton(
              child: Text(
                cancel,
                style: TextStyle(color: Colors.grey[800]),
              ),
              onPressed: onCancel),
          FlatButton(
              child: Text(
                ok,
                style: TextStyle(color: Colors.yellow[900]),
              ),
              onPressed: onOk),
        ],
      );
    }
    return dialog;
  }
}
