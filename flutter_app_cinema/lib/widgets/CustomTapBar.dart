import 'package:flutter/material.dart';

import '../views/home/HomeViewModel.dart';

class CustomTapBar extends StatelessWidget {
  HomeViewModel model;

  CustomTapBar(this.model);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        GestureDetector(
          onTap: model.onChangeColor1,
          child: model.chnageColor1
              ? Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).textTheme.display1.color,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Discover",
                    style: TextStyle(color: Colors.grey[800], fontSize: 15),
                  ),
                )
              : Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).textTheme.display2.color,
                          offset: Offset(0.0, 0.0),
                          blurRadius: 9.0,
                        )
                      ],
                      color: Colors.yellow[900],
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(14),
                  child: Text(
                    "Discover",
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
        ),
        SizedBox(
          width: 15,
        ),
        GestureDetector(
          onTap: model.onChangeColor2,
          child: model.chnageColor2
              ? Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).textTheme.display1.color,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "Upcoming",
                    style: TextStyle(color: Colors.grey[800], fontSize: 15),
                  ),
                )
              : Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).textTheme.display2.color,
                          offset: Offset(1.0, 1.0),
                          blurRadius: 6.0,
                        )
                      ],
                      color: Colors.yellow[900],
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(14),
                  child: Text(
                    "Upcoming",
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
        ),
        SizedBox(
          width: 15,
        ),
        GestureDetector(
          onTap: model.onChangeColor3,
          child: model.chnageColor3
              ? Container(
                  decoration: BoxDecoration(
                      color: Theme.of(context).textTheme.display1.color,
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(10),
                  child: Text(
                    "WatchList",
                    style: TextStyle(color: Colors.grey[800], fontSize: 15),
                  ),
                )
              : Container(
                  decoration: BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Theme.of(context).textTheme.display2.color,
                          offset: Offset(1.0, 1.0),
                          blurRadius: 6.0,
                        )
                      ],
                      color: Colors.yellow[900],
                      borderRadius: BorderRadius.all(Radius.circular(20))),
                  padding: EdgeInsets.all(14),
                  child: Text(
                    "WatchList",
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
        ),
      ],
    );
  }
}
